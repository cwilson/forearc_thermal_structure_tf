<?xml version='1.0' encoding='utf-8'?>
<harness_options>
  <length>
    <string_value lines="1">short</string_value>
  </length>
  <owner>
    <string_value lines="1">cwilson</string_value>
  </owner>
  <description>
    <string_value lines="1">Subduction forearc simulation</string_value>
  </description>
  <simulations>
    <simulation name="Subduction">
      <input_file>
        <string_value type="filename" lines="1">subduction.tfml</string_value>
      </input_file>
      <run_when name="input_changed_or_output_missing"/>
      <parameter_sweep>
        <parameter name="delta">
          <values>
            <string_value lines="1">45</string_value>
          </values>
          <update>
            <string_value type="code" language="python3" lines="20">import libspud
from math import pi
libspud.set_option("/system::Temperature/coefficient::SlabDip/type/rank/value/constant", float(delta)*pi/180.)

detectors = "def val():\n  import numpy as np\n  delta = {}\n  zs = np.linspace(0, 100000., 101)\n  coords = [[z/np.tan(delta), -z] for z in zs]\n  return coords".format(float(delta)*pi/180.,)
libspud.set_option("/io/detectors/array::Slab/python", detectors)</string_value>
            <single_build/>
          </update>
        </parameter>
        <parameter name="mu0">
          <values>
            <string_value lines="1">0.0 0.01 0.02 0.03 0.04 0.05</string_value>
          </values>
          <update>
            <string_value type="code" language="python3" lines="20">import libspud
libspud.set_option("/system::Temperature/coefficient::ApparentFrictionCoefficient/type/rank/value/constant", float(mu0))</string_value>
            <single_build/>
          </update>
        </parameter>
        <parameter name="vslab">
          <values>
            <string_value lines="1">5.0</string_value>
            <comment>cm/yr</comment>
          </values>
          <update>
            <string_value type="code" language="python3" lines="20">import libspud

libspud.set_option("/system::Temperature/coefficient::SlabSpeed/type/rank/value/constant", float(vslab)*0.01 / (365.25 * 24.0 * 3600.0))</string_value>
            <single_build/>
          </update>
        </parameter>
      </parameter_sweep>
      <dependencies>
        <run name="Mesh">
          <input_file>
            <string_value type="filename" lines="1">../mesh/meshparams.json</string_value>
            <json_file/>
          </input_file>
          <run_when name="input_changed_or_output_missing"/>
          <parameter_sweep>
            <parameter name="delta">
              <update>
                <string_value type="code" language="python3" lines="20">input_dict["delta"] = delta</string_value>
              </update>
            </parameter>
          </parameter_sweep>
          <required_output>
            <filenames name="meshfiles">
              <python>
                <string_value type="code" language="python3" lines="20">meshfiles = ['custom_subduction_mesh'+ext for ext in ['.xdmf', '.h5', '_facet_ids.xdmf', '_facet_ids.h5', '_cell_ids.xdmf', '_cell_ids.h5']]</string_value>
              </python>
            </filenames>
          </required_output>
          <commands>
            <command name="PWD">
              <string_value lines="1">dirname $input_filename</string_value>
            </command>
            <command name="Generate">
              <string_value lines="1">forearc_mesh_generator.py --delta $delta</string_value>
            </command>
          </commands>
        </run>
      </dependencies>
      <variables>
        <variable name="det">
          <string_value type="code" language="python3" lines="20">from buckettools.statfile import parser
import os
from buckettools.threadlibspud import *

filename = os.path.split(input_filename)[-1]
threadlibspud.load_options(filename)
basename = libspud.get_option("/io/output_base_name")
threadlibspud.clear_options()
det = parser(basename+".det")</string_value>
        </variable>
        <variable name="havedisplay">
          <string_value type="code" language="python3" lines="20">import os

exitval = os.system('python -c "import matplotlib.pyplot as plt; plt.figure()"')
havedisplay = (exitval == 0) and "DISPLAY" in os.environ</string_value>
        </variable>
      </variables>
    </simulation>
  </simulations>
  <tests>
    <test name="plot">
      <string_value type="code" language="python3" lines="20">import matplotlib
if havedisplay[0]:
  matplotlib.use('GTK3Agg')
else:
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os
import itertools

params = list(det.parameters.keys())
fig = plt.figure()
for values in itertools.product(*det.parameters.values()):
  label = ", ".join(["{} = {}".format(params[i], v) for i,v in enumerate(values) if len(det.parameters[params[i]])&gt;1])
  key = {params[i]:v for i,v in enumerate(values)}
  z = -det[key]['Slab']['position_1'][:,-1]/1000.
  T = det[key]['Temperature']['Temperature']['Slab'][:,-1]
  plt.plot(T, z, linewidth=2, label=label)
  z40 = (abs(z - 40)).argmin()
  print("T(z={}): ".format(z[z40])+label+": {}".format(T[z40]))

fig.savefig("subduction_forearc_T.png")
plt.gca().legend()
plt.gca().set_ylim([0.0, 80.0])
plt.gca().set_xlim([0.0, 700.0])
plt.xlabel(r"T ($^\circ$C)")
plt.ylabel(r"z (km)")</string_value>
    </test>
    <test name="display">
      <string_value type="code" language="python3" lines="20">import matplotlib
if havedisplay[0]:
  matplotlib.use('GTK3Agg')
else:
  matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os

if havedisplay[0]: plt.show()</string_value>
    </test>
  </tests>
</harness_options>
